package repository.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.dao.ProductDao;
import repository.dao.exception.ProductException;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.List;

/**
 * Реализация ProductDao для Hibernate.
 */
class HibernateProductDao implements ProductDao {
  @Override
  public List<Product> getAll() throws ProductException {
    Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();
    List<Product> products = session.createQuery("From Product", Product.class).list();
    session.close();
    return products;
  }

  @Override
  public Product getById(int productId) throws ProductException {
    Product product = HibernateDaoFactory
            .getSessionFactory()
            .openSession()
            .get(Product.class, productId);
    if (product == null) {
      throw new ProductException("Invalid id");
    }
    return product;
  }

  @Override
  public int add(Product product) throws ProductException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.beginTransaction();
    session.save(product);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new ProductException("Duplicate", e);
    }
    session.close();
    return product.getId();
  }

  @Override
  public void update(Product product) throws ProductException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.beginTransaction();
    session.update(product);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new ProductException("Update error", e);
    }
    session.close();
  }

  @Override
  public void remove(Product product) throws ProductException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.getTransaction();
    transaction.begin();
    session.delete(product);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new ProductException("Failed", e);
    }
    session.close();
  }

  @Override
  public List<Product> getByNameString(String name) throws ProductException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    List<Product> products = session
            .createQuery("From Product p where upper(p.name) like upper( :name )", Product.class)
            .setParameter("name", '%' + name + '%')
            .list();
    session.close();
    return products;
  }

  @Override
  public List<Product> getByDescriptionString(String name) throws ProductException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    List<Product> products = session
            .createQuery("From Product p where upper(p.description) like upper( :name )", Product.class)
            .setParameter("name", '%' + name + '%')
            .list();
    session.close();
    return products;
  }

  @Override
  public List<Product> getUserProducts(User user) throws ProductException {
    return HibernateDaoFactory
            .getSessionFactory()
            .openSession()
            .createQuery("From Product p where p.user.userId = :userId", Product.class)
            .setParameter("userId", user.getUserId())
            .list();
  }
}
