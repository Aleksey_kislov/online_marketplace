package repository.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.dao.UserDao;
import repository.dao.exception.UserException;
import repository.pojo.User;

import java.util.List;

/**
 * Реализация UserDao для Hibernate.
 */
class HibernateUserDao implements UserDao {
  @Override
  public List<User> getAll() throws UserException {
    List<User> userList;
    try (Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();) {
      userList = session
              .createQuery("From User", User.class)
              .list();
    } catch (HibernateException e) {
      throw new UserException("Error load. ", e);
    }
    return userList;
  }

  @Override
  public int add(User user) throws UserException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.beginTransaction();
    session.save(user);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new UserException("Duplicate login.", e);
    }
    session.close();
    return user.getUserId();
  }

  @Override
  public void update(User user) throws UserException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.beginTransaction();
    session.update(user);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new UserException("Update error", e);
    }
    session.close();
  }

  @Override
  public void remove(User user) throws UserException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.getTransaction();
    transaction.begin();
    session.delete(user);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new UserException("Failed", e);
    }
    session.close();
  }

  @Override
  public User getById(int id) throws UserException {
    Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();
    User user = session.get(User.class, id);
    if (user == null) {
      throw new UserException("Invalid id");
    }
    session.close();
    return user;
  }

  @Override
  public User getByLogin(String login) throws UserException {
    Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();
    User user = session
            .byNaturalId(User.class)
            .using("login", login)
            .load();
    session.close();
    if (user == null) {
      throw new UserException("Invalid login");
    }
    return user;
  }
}
