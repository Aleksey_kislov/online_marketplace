package repository.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import repository.dao.BetDao;
import repository.dao.exception.BetException;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.List;

/**
 * Реализация BetDao для Hibernate.
 */
class HibernateBetDao implements BetDao {
  @Override
  public int add(Bet bet) throws BetException {
    Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();
    Transaction transaction = session.beginTransaction();
    session.save(bet);
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new BetException("Error", e);
    }
    session.close();
    return bet.getBetId();
  }

  @Override
  public List<Bet> getByProduct(Product product) throws BetException {
    return getByProductId(product.getId());
  }

  @Override
  public List<Bet> getByProductId(int productId) throws BetException {
    Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();

    List<Bet> betList = session.createQuery("From Bet b where b.productId = :productId", Bet.class)
            .setParameter("productId", productId)
            .list();
    session.close();

    return betList;
  }

  @Override
  public List<Bet> getByUser(User user) throws BetException {
    return getByUserId(user.getUserId());
  }

  @Override
  public List<Bet> getByUserId(int userId) throws BetException {
    Session session = HibernateDaoFactory
            .getSessionFactory()
            .openSession();

    List<Bet> betList = session.createQuery("From Bet b where b.userId = :userId", Bet.class)
            .setParameter("userId", userId)
            .list();
    session.close();

    return betList;
  }

  @Override
  public void removeByProduct(Product product) throws BetException {
    Session session = HibernateDaoFactory.getSessionFactory().openSession();
    Transaction transaction = session.beginTransaction();
    Query query = session.createQuery("delete Bet where productId = :productId");
    query.setParameter("productId", product.getId());
    query.executeUpdate();
    try {
      transaction.commit();
    } catch (Exception e) {
      throw new BetException("Failed", e);
    }
    session.close();
  }
}
