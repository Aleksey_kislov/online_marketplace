package repository.dao.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import repository.dao.BetDao;
import repository.dao.DaoFactory;
import repository.dao.ProductDao;
import repository.dao.UserDao;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.Locale;

/**
 * Реализация DaoFactory для Hibernate.
 */
public class HibernateDaoFactory extends DaoFactory {

  private static SessionFactory sessionFactory;

  /**
   * Метод для получения SessionFactory.
   *
   * @return - возвращает SessionFactory.
   */
  static SessionFactory getSessionFactory() {
    if (sessionFactory == null) {
      try {
        Locale.setDefault(Locale.ENGLISH);
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Product.class);
        configuration.addAnnotatedClass(Bet.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());

      } catch (Exception e) {
        System.out.println("Исключение!" + e);
      }
    }
    return sessionFactory;
  }

  @Override
  public UserDao getUserDao() {
    return new HibernateUserDao();
  }

  @Override
  public ProductDao getProductDao() {
    return new HibernateProductDao();
  }

  @Override
  public BetDao getBetDao() {
    return new HibernateBetDao();
  }
}
