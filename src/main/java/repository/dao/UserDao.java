package repository.dao;

import repository.dao.exception.UserException;
import repository.pojo.User;

import java.util.List;


/**
 * Интерфейс для взаимодествия с хранилищем данных о пользователе.
 */
public interface UserDao {
  /**
   * Получить список всех пользователей.
   *
   * @return - список пользователей.
   * @throws UserException - ошибка получения пользователей.
   */
  List<User> getAll() throws UserException;

  /**
   * Добавление добавление пользователя в хранилище данных.
   *
   * @param user - пользователь которого надо добавить.
   * @return - авто-сгенерированный ID пользователя.
   * @throws UserException - ошибка при добавлении.
   */
  int add(User user) throws UserException;

  /**
   * Обновление данных пользователя в хранилище.
   *
   * @param user - пользователь для обновления.
   * @throws UserException - ошибка обновления.
   */
  void update(User user) throws UserException;

  /**
   * Удаляет пользователя из хранилища.
   *
   * @param user - пользователь которого надо удалить.
   * @throws UserException - ошибка удаления.
   */
  void remove(User user) throws UserException;

  /**
   * Получение пользователя по его ID.
   *
   * @param id - id пользователя.
   * @return - пользователь с данным id.
   * @throws UserException - данного пользователя нет.
   */
  User getById(int id) throws UserException;

  /**
   * Получение пользователя по его Login'у.
   *
   * @param login - логин пользователя.
   * @return - пользователя с данным логином.
   * @throws UserException - данного пользователя нет.
   */
  User getByLogin(String login) throws UserException;
}
