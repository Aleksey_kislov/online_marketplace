package repository.dao;

import repository.dao.jdbc.OracleDaoFactory;

/**
 * Абстрактный класс реализующий паттерн DaoFactory.
 * Служит для получения доступа к конкретным реализациям Dao объектов.
 */
public abstract class DaoFactory {
  /**
   * Идентификатор Oracle реализации repository.dao.
   */
  public static final int ORACLE = 1;

  /**
   * Получение определенного DaoFactory по его идентификатору.
   *
   * @param whichFactory - идентификатор.
   * @return - конкретная реализация DaoFactory
   */
  public static DaoFactory getDaoFactory(int whichFactory) {
    switch (whichFactory) {
      case ORACLE:
        return new OracleDaoFactory();
      default:
        return null;
    }
  }

  /**
   * Абстрактный метод для получения конкретной реализации UserDao.
   *
   * @return - возвращает конкретную реализацию UserDao.
   */
  public abstract UserDao getUserDao();

  /**
   * Абстрактный метод для получения конкретной реализации ProductDao.
   *
   * @return - возвращает конкретную реализацию ProductDao.
   */
  public abstract ProductDao getProductDao();

  /**
   * Абстрактный метод для получения конкретной реализации BetDao.
   *
   * @return - возвращает конкретную реализацию BetDao.
   */
  public abstract BetDao getBetDao();
}
