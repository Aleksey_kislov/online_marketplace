package repository.dao;

import repository.dao.exception.BetException;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.List;

/**
 * Интерфейс для взаимодействия с хранилищем данных ставок.
 */
public interface BetDao {
  /**
   * Метод для добвления новых ставок.
   *
   * @param bet - объект ставки.
   * @return - возвращает id ставки.
   * @throws BetException - ошибка добавления ставки.
   */
  int add(Bet bet) throws BetException;

  /**
   * Получить список ставок на продукт.
   *
   * @param product - продукт по которому находятся ставки.
   * @return - возвращает список ставок.
   * @throws BetException - ошибка получения ставок.
   */
  List<Bet> getByProduct(Product product) throws BetException;

  /**
   * Получить список ставок на продукт по его ID.
   *
   * @param productId - id продукта.
   * @return - возвращает список ставок.
   * @throws BetException - ошибка получения ставок.
   */
  List<Bet> getByProductId(int productId) throws BetException;

  /**
   * Получение списка ставок сделанных пользователем.
   *
   * @param user - пользователь по коорому находятся ставки.
   * @return - возвращает список ставок сделанных пользователем.
   * @throws BetException - ошибка получения ставок.
   */
  List<Bet> getByUser(User user) throws BetException;

  /**
   * Получение списка ставок сделанных пользователем по его ID.
   *
   * @param userId - ID пользователя по которому находится ставка.
   * @return - возвращает список ставок сделанных пользователем.
   * @throws BetException - ошибка получения ставок.
   */
  List<Bet> getByUserId(int userId) throws BetException;

  /**
   * Удаление ставок по товару.
   *
   * @param product - товар по которому происходит удаление.
   * @throws BetException - ошибка удаления товара.
   */
  void removeByProduct(Product product) throws BetException;
}
