package repository.dao.exception;

/**
 * Exception для BetDao.
 */
public class BetException extends Exception {
  public BetException() {
  }

  public BetException(String message) {
    super(message);
  }

  public BetException(String message, Throwable cause) {
    super(message, cause);
  }

  public BetException(Throwable cause) {
    super(cause);
  }

  public BetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
