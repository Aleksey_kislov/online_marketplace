package repository.dao;

import repository.dao.exception.ProductException;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.List;

/**
 * Интерфейс для взаимодействия с хранилищем данных товаров.
 */
public interface ProductDao {
  /**
   * Получение всех товаров.
   *
   * @return - список продуктов.
   * @throws ProductException - ошибка получение продуктов.
   */
  List<Product> getAll() throws ProductException;

  /**
   * Получение товара по id.
   *
   * @param productId - id товара.
   * @return - товар.
   * @throws ProductException - товар отсутствует.
   */
  Product getById(int productId) throws ProductException;

  /**
   * Добавление товара в хранилище.
   *
   * @param product - товар для добавления.
   * @return - сгенерированный id товара.
   * @throws ProductException - ошибка добавления товара.
   */
  int add(Product product) throws ProductException;

  /**
   * Обновление товара.
   *
   * @param product - измененный товар.
   * @throws ProductException - ошибка изменения.
   */
  void update(Product product) throws ProductException;

  /**
   * Удаление товара из хранилища.
   *
   * @param product - товар который нужно удалить.
   * @throws ProductException - ошибка удаления товара.
   */
  void remove(Product product) throws ProductException;

  /**
   * Получение товаров по части названия товара.
   *
   * @param name - часть названия товара.
   * @return - список товаров.
   * @throws ProductException - ошибка получения товара по части названия.
   */
  List<Product> getByNameString(String name) throws ProductException;

  /**
   * Получение товаров по части описания товара.
   *
   * @param name - часть описания товара.
   * @return - список товаров.
   * @throws ProductException - ошибка получения товара по части опичания.
   */
  List<Product> getByDescriptionString(String name) throws ProductException;

  /**
   * Получение всех товаров определенного пользователя.
   *
   * @param user - пользователь товары которого надо получить.
   * @return - список товаров.
   * @throws ProductException - ошибка получения товаров.
   */
  List<Product> getUserProducts(User user) throws ProductException;


}
