package repository.dao.jdbc;

import repository.dao.BetDao;
import repository.dao.DaoFactory;
import repository.dao.ProductDao;
import repository.dao.UserDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;

/**
 * Реализация DaoFactory для Oracle СУБД.
 */
public class OracleDaoFactory extends DaoFactory {
  private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
  private static final String DB_URL = "jdbc:oracle:thin:@localhost:1521:xe";
  private static final int TIME_OUT = 100;
  private static Connection connection;

  /**
   * Метод для получения Connection к Oracle DB.
   *
   * @return - возвращает Connection.
   * @throws SQLException - Ошибка Sql.
   */
  public static Connection createConnection() throws SQLException {
    if (connection == null) {
      try {
        Class.forName(DRIVER);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      Locale.setDefault(Locale.ENGLISH);
      connection = DriverManager.getConnection(
              DB_URL, "MARKETPLACE", "MARKETPLACE");
    }
    return connection;
  }

  @Override
  public UserDao getUserDao() {
    return new OracleUserDao();
  }

  @Override
  public ProductDao getProductDao() {
    return new OracleProductDao();
  }

  @Override
  public BetDao getBetDao() {
    return new OracleBetDao();
  }
}
