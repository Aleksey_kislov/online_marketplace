package repository.dao.jdbc;

import oracle.jdbc.OracleTypes;
import repository.dao.BetDao;
import repository.dao.exception.BetException;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Реализация BetDao для Oracle data base.
 */
class OracleBetDao implements BetDao {
  private static final String TABLE_NAME = "Bets";
  private static final String BET_ID = "betId";
  private static final String USER_ID = "userId";
  private static final String PRODUCT_ID = "productId";
  private static final String BET = "bet";

  @Override
  public int add(Bet bet) throws BetException {
    CallableStatement callableStatement = null;
    int id = -1;
    try {
      callableStatement = OracleDaoFactory
              .createConnection()
              .prepareCall("BEGIN INSERT INTO " + TABLE_NAME + " ("
                           + PRODUCT_ID + ", "
                           + USER_ID + ", "
                           + BET + ") "
                           + "VALUES (?,?,?) returning " + BET_ID + " INTO ?; END;");

      callableStatement.setInt(1, bet.getProductId());
      callableStatement.setInt(2, bet.getUserId());
      callableStatement.setFloat(3, bet.getBet());
      callableStatement.registerOutParameter(4, OracleTypes.NUMBER);
      callableStatement.executeUpdate();
      id = callableStatement.getInt(4);

      callableStatement.close();
    } catch (SQLException e) {
      throw new BetException("Create bet failed", e);
    } finally {
      if (callableStatement != null) {
        try {
          callableStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return id;
  }

  @Override
  public List<Bet> getByProduct(Product product) throws BetException {
    return getByProductId(product.getId());
  }

  @Override
  public List<Bet> getByProductId(int productId) throws BetException {
    List<Bet> betList = new ArrayList<>();
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("SELECT " + BET_ID + ", "
                              + PRODUCT_ID + ", "
                              + BET + ", "
                              + USER_ID + " "
                              + "FROM " + TABLE_NAME + " "
                              + "WHERE " + PRODUCT_ID + "= ?"
                              + "ORDER BY " + BET + " DESC")) {

      preparedStatement.setInt(1, productId);
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        Bet bet = new Bet();
        bet.setBetId(rs.getInt(BET_ID));
        bet.setProductId(rs.getInt(PRODUCT_ID));
        bet.setUserId(rs.getInt(USER_ID));
        bet.setBet(rs.getFloat(BET));
        betList.add(bet);
      }
    } catch (SQLException e) {
      throw new BetException("failed load bets.", e);
    }
    return betList;
  }

  @Override
  public List<Bet> getByUser(User user) throws BetException {
    return getByUserId(user.getUserId());
  }

  @Override
  public List<Bet> getByUserId(int userId) throws BetException {
    List<Bet> betList = new ArrayList<>();
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("SELECT " + BET_ID + ", "
                              + PRODUCT_ID + ", "
                              + BET + ", "
                              + USER_ID + " "
                              + "FROM " + TABLE_NAME + " "
                              + "WHERE " + USER_ID + "= ?")) {

      preparedStatement.setInt(1, userId);
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        Bet bet = new Bet();
        bet.setBetId(rs.getInt(BET_ID));
        bet.setProductId(rs.getInt(PRODUCT_ID));
        bet.setUserId(rs.getInt(USER_ID));
        bet.setBet(rs.getFloat(BET));
        betList.add(bet);
      }
    } catch (SQLException e) {
      throw new BetException("failed load bets.", e);
    }
    return betList;
  }

  @Override
  public void removeByProduct(Product product) throws BetException {
    PreparedStatement preparedStatement = null;
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("DELETE FROM " + TABLE_NAME
                                + " WHERE " + PRODUCT_ID + " = ?");
      preparedStatement.setInt(1, product.getId());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new BetException("product not found", e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
