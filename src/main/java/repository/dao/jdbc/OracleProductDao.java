package repository.dao.jdbc;

import oracle.jdbc.OracleTypes;
import repository.dao.ProductDao;
import repository.dao.exception.ProductException;
import repository.pojo.Product;
import repository.pojo.User;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Реализация ProductDao для Oracle data base.
 */
class OracleProductDao implements ProductDao {
  private static final String TABLE_NAME = "Products";
  private static final String PRODUCT_ID = "id";
  private static final String DESCRIPTION = "description";
  private static final String NAME = "name";
  private static final String START_BET = "startBet";
  private static final String BETS_STEP = "betStep";
  private static final String TIME_PER_LOT = "timePerLot";
  private static final String BUY_NOW = "buyNow";
  private static final String TO_BETS = "toBets";
  private static final String USER_ID = "userId";

  @Override
  public List<Product> getAll() throws ProductException {
    PreparedStatement preparedStatement = null;
    List<Product> productList = new ArrayList<>();
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("SELECT "
                                + PRODUCT_ID + ", "
                                + DESCRIPTION + ", "
                                + NAME + ", "
                                + START_BET + ", "
                                + BETS_STEP + ", "
                                + TIME_PER_LOT + ", "
                                + BUY_NOW + ", "
                                + TO_BETS + ", "
                                + USER_ID + " "
                                + "FROM " + TABLE_NAME);

      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        Product product = new Product();
        product.setId(rs.getInt(PRODUCT_ID));
        product.setDescription(rs.getString(DESCRIPTION));
        product.setName(rs.getString(NAME));
        product.setStartBet(rs.getFloat(START_BET));
        product.setBetStep(rs.getFloat(BETS_STEP));
        product.setTimePerLot(rs.getTimestamp(TIME_PER_LOT).toLocalDateTime());
        product.setBuyNow(rs.getFloat(BUY_NOW));
        product.setToBets(rs.getBoolean(TO_BETS));
        User user = new User();
        user.setUserId(rs.getInt(USER_ID));
        product.setUser(user);
        productList.add(product);
      }
    } catch (SQLException e) {
      throw new ProductException(e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return productList;
  }

  @Override
  public Product getById(int id) throws ProductException {
    PreparedStatement preparedStatement = null;
    Product product;
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("SELECT "
                                + PRODUCT_ID + ", "
                                + DESCRIPTION + ", "
                                + NAME + ", "
                                + START_BET + ", "
                                + BETS_STEP + ", "
                                + TIME_PER_LOT + ", "
                                + BUY_NOW + ", "
                                + TO_BETS + ", "
                                + USER_ID + " "
                                + " FROM " + TABLE_NAME
                                + " WHERE " + PRODUCT_ID + " = ?");
      preparedStatement.setInt(1, id);
      ResultSet rs = preparedStatement.executeQuery();
      rs.next();
      product = new Product();
      product.setId(rs.getInt(PRODUCT_ID));
      product.setDescription(rs.getString(DESCRIPTION));
      product.setName(rs.getString(NAME));
      product.setStartBet(rs.getFloat(START_BET));
      product.setBetStep(rs.getFloat(BETS_STEP));
      product.setTimePerLot(rs.getTimestamp(TIME_PER_LOT).toLocalDateTime());
      product.setBuyNow(rs.getFloat(BUY_NOW));
      product.setToBets(rs.getBoolean(TO_BETS));
      User user = new User();
      user.setUserId(rs.getInt(USER_ID));
      product.setUser(user);
    } catch (SQLException e) {
      throw new ProductException(e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return product;
  }

  @Override
  public int add(Product product) throws ProductException {
    CallableStatement callableStatement = null;
    int id = -1;
    try {
      callableStatement = OracleDaoFactory
              .createConnection()
              .prepareCall("BEGIN INSERT INTO " + TABLE_NAME + " ("
                           + DESCRIPTION + ", "
                           + NAME + ", "
                           + START_BET + ", "
                           + BETS_STEP + ", "
                           + TIME_PER_LOT + ", "
                           + BUY_NOW + ", "
                           + TO_BETS + ", "
                           + USER_ID + ") "
                           + "VALUES (?,?,?,?,?,?,?,?) returning " + PRODUCT_ID + " INTO ?; END;");

      callableStatement.setString(1, product.getDescription());
      callableStatement.setString(2, product.getName());
      callableStatement.setFloat(3, product.getStartBet());
      callableStatement.setFloat(4, product.getBetStep());
      callableStatement.setTimestamp(5, Timestamp.valueOf(product.getTimePerLot()));
      callableStatement.setFloat(6, product.getBuyNow());
      callableStatement.setBoolean(7, product.getToBets());
      callableStatement.setInt(8, product.getUser().getUserId());
      callableStatement.registerOutParameter(9, OracleTypes.NUMBER);
      callableStatement.executeUpdate();
      id = callableStatement.getInt(9);

      callableStatement.close();
    } catch (SQLException e) {
      throw new ProductException("Create product failed", e);
    } finally {
      if (callableStatement != null) {
        try {
          callableStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return id;
  }

  @Override
  public void update(Product product) throws ProductException {
    PreparedStatement preparedStatement = null;
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("UPDATE " + TABLE_NAME + " SET "
                                + DESCRIPTION + "=?, "
                                + NAME + "=?, "
                                + START_BET + "=?, "
                                + BETS_STEP + "=?, "
                                + TIME_PER_LOT + "=?, "
                                + BUY_NOW + "=?, "
                                + TO_BETS + "=?, "
                                + USER_ID + "=? "
                                + " WHERE " + PRODUCT_ID + " = ?");

      preparedStatement.setString(1, product.getDescription());
      preparedStatement.setString(2, product.getName());
      preparedStatement.setFloat(3, product.getStartBet());
      preparedStatement.setFloat(4, product.getBetStep());
      preparedStatement.setTimestamp(5, Timestamp.valueOf(product.getTimePerLot()));
      preparedStatement.setFloat(6, product.getBuyNow());
      preparedStatement.setBoolean(7, product.getToBets());
      preparedStatement.setInt(8, product.getUser().getUserId());
      preparedStatement.setInt(9, product.getId());

      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new ProductException("Failed update", e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void remove(Product product) throws ProductException {
    PreparedStatement preparedStatement = null;
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("DELETE FROM " + TABLE_NAME
                                + " WHERE " + PRODUCT_ID + " = ?");
      preparedStatement.setInt(1, product.getId());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new ProductException("product not found", e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public List<Product> getByNameString(String name) throws ProductException {
    PreparedStatement preparedStatement = null;
    List<Product> productList = new ArrayList<>();
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("SELECT "
                                + PRODUCT_ID + ", "
                                + DESCRIPTION + ", "
                                + NAME + ", "
                                + START_BET + ", "
                                + BETS_STEP + ", "
                                + TIME_PER_LOT + ", "
                                + BUY_NOW + ", "
                                + TO_BETS + ", "
                                + USER_ID + " "
                                + " FROM " + TABLE_NAME
                                + " WHERE UPPER(" + NAME + ") LIKE UPPER(?)");
      preparedStatement.setString(1, "%" + name + "%");

      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        Product product = new Product();
        product.setId(rs.getInt(PRODUCT_ID));
        product.setDescription(rs.getString(DESCRIPTION));
        product.setName(rs.getString(NAME));
        product.setStartBet(rs.getFloat(START_BET));
        product.setBetStep(rs.getFloat(BETS_STEP));
        product.setTimePerLot(rs.getTimestamp(TIME_PER_LOT).toLocalDateTime());
        product.setBuyNow(rs.getFloat(BUY_NOW));
        product.setToBets(rs.getBoolean(TO_BETS));
        User user = new User();
        user.setUserId(rs.getInt(USER_ID));
        product.setUser(user);
        productList.add(product);
      }
    } catch (SQLException e) {
      throw new ProductException(e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return productList;
  }

  @Override
  public List<Product> getByDescriptionString(String name) throws ProductException {
    PreparedStatement preparedStatement = null;
    List<Product> productList = new ArrayList<>();
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("SELECT "
                                + PRODUCT_ID + ", "
                                + DESCRIPTION + ", "
                                + NAME + ", "
                                + START_BET + ", "
                                + BETS_STEP + ", "
                                + TIME_PER_LOT + ", "
                                + BUY_NOW + ", "
                                + TO_BETS + ", "
                                + USER_ID + " "
                                + " FROM " + TABLE_NAME
                                + " WHERE UPPER(" + DESCRIPTION + ") LIKE UPPER(?)");
      preparedStatement.setString(1, "%" + name + "%");

      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        Product product = new Product();
        product.setId(rs.getInt(PRODUCT_ID));
        product.setDescription(rs.getString(DESCRIPTION));
        product.setName(rs.getString(NAME));
        product.setStartBet(rs.getFloat(START_BET));
        product.setBetStep(rs.getFloat(BETS_STEP));
        product.setTimePerLot(rs.getTimestamp(TIME_PER_LOT).toLocalDateTime());
        product.setBuyNow(rs.getFloat(BUY_NOW));
        product.setToBets(rs.getBoolean(TO_BETS));
        User user = new User();
        user.setUserId(rs.getInt(USER_ID));
        product.setUser(user);
        productList.add(product);
      }
    } catch (SQLException e) {
      throw new ProductException(e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return productList;
  }

  @Override
  public List<Product> getUserProducts(User user) throws ProductException {
    PreparedStatement preparedStatement = null;
    List<Product> productList = new ArrayList<>();
    try {
      preparedStatement = OracleDaoFactory
              .createConnection()
              .prepareStatement("SELECT "
                                + PRODUCT_ID + ", "
                                + DESCRIPTION + ", "
                                + NAME + ", "
                                + START_BET + ", "
                                + BETS_STEP + ", "
                                + TIME_PER_LOT + ", "
                                + BUY_NOW + ", "
                                + TO_BETS + " "
                                + " FROM " + TABLE_NAME
                                + " WHERE " + USER_ID + " = ? ");
      preparedStatement.setInt(1, user.getUserId());

      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        Product product = new Product();
        product.setId(rs.getInt(PRODUCT_ID));
        product.setDescription(rs.getString(DESCRIPTION));
        product.setName(rs.getString(NAME));
        product.setStartBet(rs.getFloat(START_BET));
        product.setBetStep(rs.getFloat(BETS_STEP));
        product.setTimePerLot(rs.getTimestamp(TIME_PER_LOT).toLocalDateTime());
        product.setBuyNow(rs.getFloat(BUY_NOW));
        product.setToBets(rs.getBoolean(TO_BETS));
        product.setUser(user);
        productList.add(product);
      }
    } catch (SQLException e) {
      throw new ProductException(e);
    } finally {
      if (preparedStatement != null) {
        try {
          preparedStatement.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return productList;
  }
}
