package repository.dao.jdbc;

import oracle.jdbc.OracleTypes;
import repository.dao.UserDao;
import repository.dao.exception.UserException;
import repository.pojo.User;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Реализация UserDao для Oracle data base.
 */
class OracleUserDao implements UserDao {

  private static final String TABLE_NAME = "Users";
  private static final String FIRST_NAME = "firstName";
  private static final String LAST_NAME = "lastName";
  private static final String LOGIN = "login";
  private static final String PASSWORD = "password";
  private static final String USER_ID = "userID";

  @Override
  public List<User> getAll() throws UserException {
    List<User> userList = new ArrayList<>();
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("SELECT " + FIRST_NAME + ", "
                              + LAST_NAME + ", "
                              + LOGIN + ", "
                              + PASSWORD + ", "
                              + USER_ID + " "
                              + "FROM " + TABLE_NAME)) {

      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        userList.add(new User(rs.getString(FIRST_NAME),
                rs.getString(LAST_NAME),
                rs.getString(LOGIN),
                rs.getString(PASSWORD),
                rs.getInt(USER_ID)));
      }
    } catch (SQLException e) {
      throw new UserException(e);
    }
    return userList;
  }

  @Override
  public void update(User user) throws UserException {
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("UPDATE " + TABLE_NAME + " SET "
                              + FIRST_NAME + "=?,"
                              + LAST_NAME + "=?,"
                              + PASSWORD + "=? "
                              + "WHERE " + LOGIN + " = ?")) {

      preparedStatement.setString(1, user.getFirstName());
      preparedStatement.setString(2, user.getLastName());
      preparedStatement.setString(3, user.getPassword());
      preparedStatement.setString(4, user.getLogin());
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new UserException("Update error", e);
    }
  }

  @Override
  public int add(User user) throws UserException {
    int id = -1;
    try (CallableStatement callableStatement = OracleDaoFactory
            .createConnection()
            .prepareCall("BEGIN INSERT INTO " + TABLE_NAME + " ("
                         + FIRST_NAME + ", "
                         + LAST_NAME + ", "
                         + LOGIN + ", "
                         + PASSWORD + ") "
                         + "VALUES (?,?,?,?) returning " + USER_ID + " INTO ?; END;")) {
      callableStatement.setString(1, user.getFirstName());
      callableStatement.setString(2, user.getLastName());
      callableStatement.setString(3, user.getLogin());
      callableStatement.setString(4, user.getPassword());
      callableStatement.registerOutParameter(5, OracleTypes.NUMBER);
      callableStatement.executeUpdate();
      id = callableStatement.getInt(5);
    } catch (SQLException e) {
      throw new UserException("Duplicate login", e);
    }
    return id;
  }

  @Override
  public User getById(int id) throws UserException {
    User user = null;
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("SELECT " + FIRST_NAME + ", "
                              + LAST_NAME + ", "
                              + LOGIN + ", "
                              + PASSWORD + ", "
                              + USER_ID + " "
                              + "FROM " + TABLE_NAME + " "
                              + "WHERE " + USER_ID + "= ?")) {

      preparedStatement.setInt(1, id);
      ResultSet rs = preparedStatement.executeQuery();
      rs.next();
      user = new User(rs.getString(FIRST_NAME),
              rs.getString(LAST_NAME),
              rs.getString(LOGIN),
              rs.getString(PASSWORD),
              rs.getInt(USER_ID));
    } catch (SQLException e) {
      throw new UserException("Invalid id", e);
    }
    return user;
  }

  @Override
  public User getByLogin(String login) throws UserException {
    User user = null;
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("SELECT " + FIRST_NAME + ", "
                              + LAST_NAME + ", "
                              + LOGIN + ", "
                              + PASSWORD + ", "
                              + USER_ID + " "
                              + "FROM " + TABLE_NAME + " "
                              + "WHERE " + LOGIN + "= ?")) {

      preparedStatement.setString(1, login);
      ResultSet rs = preparedStatement.executeQuery();
      rs.next();
      user = new User(rs.getString(FIRST_NAME),
              rs.getString(LAST_NAME),
              rs.getString(LOGIN),
              rs.getString(PASSWORD),
              rs.getInt(USER_ID));
    } catch (SQLException e) {
      throw new UserException("Invalid login", e);
    }
    return user;
  }

  @Override
  public void remove(User user) throws UserException {
    try (PreparedStatement preparedStatement = OracleDaoFactory
            .createConnection()
            .prepareStatement("DELETE FROM " + TABLE_NAME
                              + " WHERE " + LOGIN + " = '" + user.getLogin() + "'")) {
      preparedStatement.executeUpdate();
    } catch (SQLException e) {
      throw new UserException("User not found", e);
    }
  }
}
