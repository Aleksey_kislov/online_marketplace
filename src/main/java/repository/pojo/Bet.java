package repository.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "Bets")
public class Bet {

  @Column(name = "userId", nullable = false, insertable = false, updatable = false)
  private Integer userId;

  @Column(name = "productId", nullable = false, insertable = false, updatable = false)
  private Integer productId;

  @Column(name = "bet")
  private Float bet;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BETS_SEQ")
  @SequenceGenerator(name = "BETS_SEQ", sequenceName = "BETS_SEQ", allocationSize = 1)
  @Column(name = "betId", nullable = false, insertable = false, updatable = false)
  private Integer betId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userId")
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "productId", nullable = false)
  private Product product;

  public Bet() {
  }

  public Bet(int userId, int productId, float bet, int betId) {
    this.userId = userId;
    this.productId = productId;
    this.bet = bet;
    this.betId = betId;
  }

  public Bet(int userId, int productId, float bet) {
    this.userId = userId;
    this.productId = productId;
    this.bet = bet;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public Float getBet() {
    return bet;
  }

  public void setBet(Float bet) {
    this.bet = bet;
  }

  public Integer getBetId() {
    return betId;
  }

  public void setBetId(Integer betId) {
    this.betId = betId;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  @Override
  public String toString() {
    return "Bet{"
           + "userId=" + userId
           + ", productId=" + productId
           + ", bet=" + bet
           + ", betId=" + betId + '}';
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Bet bet1 = (Bet) obj;
    return Objects.equals(userId, bet1.userId)
           && Objects.equals(productId, bet1.productId)
           && Objects.equals(bet, bet1.bet)
           && Objects.equals(betId, bet1.betId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, productId, bet, betId);
  }
}
