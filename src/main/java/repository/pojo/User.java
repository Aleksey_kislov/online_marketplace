package repository.pojo;

import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Objects;

@Entity
@Table(name = "Users", uniqueConstraints = @UniqueConstraint(columnNames = {"userId", "login"}))
public class User {
  @Column(name = "firstName")
  private String firstName;

  @Column(name = "lastName")
  private String lastName;

  @Column(name = "login", nullable = false, unique = true)
  @NaturalId
  private String login;

  @Column(name = "password")
  private String password;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERSIDS_SEQ")
  @SequenceGenerator(name = "USERSIDS_SEQ", sequenceName = "USERSIDS_SEQ", allocationSize = 1)
  @Column(name = "userId", nullable = false, unique = true)
  private int userId;

  public User() {
  }

  public User(String firstName, String lastName, String login, String password) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.login = login;
    this.password = password;
  }

  public User(String firstName, String lastName, String login, String password, int userId) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.login = login;
    this.password = password;
    this.userId = userId;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  @Override
  public String toString() {
    return "User{"
           + "firstName='" + firstName + '\''
           + ", lastName='" + lastName + '\''
           + ", login='" + login + '\''
           + ", password='" + password + '\''
           + ", userId=" + userId + '}';
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || getClass() != object.getClass()) {
      return false;
    }
    User user = (User) object;
    return userId == user.userId
           && Objects.equals(firstName, user.firstName)
           && Objects.equals(lastName, user.lastName)
           && Objects.equals(login, user.login)
           && Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, login, password, userId);
  }
}
