package repository.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Products")
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCTS_SEQ")
  @SequenceGenerator(name = "PRODUCTS_SEQ", sequenceName = "PRODUCTS_SEQ", allocationSize = 1)
  @Column(name = "id", nullable = false, insertable = false, updatable = false)
  private int id;

  @Column(name = "description")
  private String description;

  @Column(name = "name")
  private String name;

  @Column(name = "startBet")
  private Float startBet;

  @Column(name = "betStep")
  private Float betStep;

  @Column(name = "timePerLot")
  private LocalDateTime timePerLot;

  @Column(name = "buyNow")
  private Float buyNow;

  @Column(name = "toBets")
  private Boolean toBets;

  @ManyToOne
  @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "USERID_FK"))
  private User user;

  @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @OrderBy("bet desc")
  private List<Bet> bets;

  public Product() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Float getStartBet() {
    return startBet;
  }

  public void setStartBet(Float startBet) {
    this.startBet = startBet;
  }

  public Float getBetStep() {
    return betStep;
  }

  public void setBetStep(Float betStep) {
    this.betStep = betStep;
  }

  public LocalDateTime getTimePerLot() {
    return timePerLot;
  }

  public void setTimePerLot(LocalDateTime timePerLot) {
    this.timePerLot = timePerLot;
  }

  public Float getBuyNow() {
    return buyNow;
  }

  public void setBuyNow(Float buyNow) {
    this.buyNow = buyNow;
  }

  public Boolean getToBets() {
    return toBets;
  }

  public void setToBets(Boolean toBets) {
    this.toBets = toBets;
  }

  public List<Bet> getBets() {
    return bets;
  }

  public void setBets(List<Bet> bets) {
    this.bets = bets;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "Product{" + "id=" + id
           + ", description='" + description
           + '\'' + ", name='" + name
           + '\'' + ", startBet=" + startBet
           + ", betStep=" + betStep
           + ", timePerLot=" + timePerLot
           + ", buyNow=" + buyNow
           + ", toBets=" + toBets
           + ", user=" + user.toString() + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Product product = (Product) o;
    return id == product.id
           && Objects.equals(description, product.description)
           && Objects.equals(name, product.name)
           && Objects.equals(startBet, product.startBet)
           && Objects.equals(betStep, product.betStep)
           && Objects.equals(timePerLot, product.timePerLot)
           && Objects.equals(buyNow, product.buyNow)
           && Objects.equals(toBets, product.toBets)
           && Objects.equals(user.getUserId(), product.user.getUserId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, description, name, startBet, betStep, timePerLot, buyNow, toBets, user);
  }
}
