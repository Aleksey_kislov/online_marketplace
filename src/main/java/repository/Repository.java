package repository;

import repository.dao.exception.BetException;
import repository.dao.exception.ProductException;
import repository.dao.exception.UserException;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.List;

/**
 * Хранилище данных.
 */
public interface Repository {
  /**
   * Создание нового пользователя.
   *
   * @param user - пользователь.
   * @return - сгенерированный id.
   * @throws UserException - ошибка создания.
   */
  int createUser(User user) throws UserException;

  /**
   * Получение пользователя по его логину.
   *
   * @param login - логин.
   * @return - пользователь с данным логином.
   * @throws UserException - пользователя не существуюет.
   */
  User getUserByLogin(String login) throws UserException;

  /**
   * Обновление данных пользователя.
   *
   * @param user - пользователь.
   * @throws UserException - ошибка обновления.
   */
  void updateUser(User user) throws UserException;

  /**
   * Создание нового продукта.
   *
   * @param product - продукт.
   * @return - сгенерированный id продукта.
   * @throws ProductException - ошибка создания продукта.
   */
  int createProduct(Product product) throws ProductException;

  /**
   * Получить список актуальных продуктов.
   *
   * @return - список продуктов.
   * @throws ProductException - ошибка получения.
   */
  List<Product> getActualProduct() throws ProductException;

  /**
   * Поиск продукта по определенной колонке.
   *
   * @param searchStr  - строка поиска.
   * @param searchType - тип поиска.
   * @return - список найденных объектов.
   * @throws ProductException - ошибка поиска.
   */
  List<Product> searchProduct(String searchStr, SearchType searchType) throws ProductException;

  /**
   * Обновленние данных продукта.
   *
   * @param product - продукт.
   * @throws ProductException ошибка обновления продукта.
   */
  void updateProduct(Product product) throws ProductException;

  /**
   * Создание ставки.
   *
   * @param bet - ставка.
   * @return - сгенерированный id.
   * @throws BetException - ошибка создания ставки.
   */
  int createBet(Bet bet) throws BetException;

  /**
   * Удаление продукта.
   *
   * @param product - продукт.
   * @throws ProductException - ошибка удаления продукта.
   */
  void removeProduct(Product product) throws ProductException;
}
