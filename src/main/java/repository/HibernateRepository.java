package repository;

import repository.dao.BetDao;
import repository.dao.DaoFactory;
import repository.dao.ProductDao;
import repository.dao.UserDao;
import repository.dao.exception.BetException;
import repository.dao.exception.ProductException;
import repository.dao.exception.UserException;
import repository.dao.hibernate.HibernateDaoFactory;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.util.ArrayList;
import java.util.List;

public class HibernateRepository implements Repository {
  private UserDao userDao;
  private ProductDao productDao;
  private BetDao betDao;

  public HibernateRepository() {
    DaoFactory daoFactory = new HibernateDaoFactory();
    userDao = daoFactory.getUserDao();
    productDao = daoFactory.getProductDao();
    betDao = daoFactory.getBetDao();
  }

  @Override
  public int createUser(User user) throws UserException {
    return userDao.add(user);
  }

  @Override
  public User getUserByLogin(String login) throws UserException {
    return userDao.getByLogin(login);
  }

  @Override
  public void updateUser(User user) throws UserException {
    userDao.update(user);
  }

  @Override
  public int createProduct(Product product) throws ProductException {
    return productDao.add(product);
  }

  @Override
  public List<Product> getActualProduct() throws ProductException {
    return productDao.getAll();
  }

  @Override
  public List<Product> searchProduct(String searchStr, SearchType searchType) throws ProductException {
    List<Product> products;
    switch (searchType) {
      case NAME:
        products = productDao.getByNameString(searchStr);
      case DESCRIPTION:
        products = productDao.getByDescriptionString(searchStr);
      case ID:
        products = new ArrayList<>();
        products.add(productDao.getById(Integer.parseInt(searchStr)));
      default:
        products = new ArrayList<>();
    }
    return products;
  }

  @Override
  public void updateProduct(Product product) throws ProductException {
    productDao.update(product);
  }

  @Override
  public int createBet(Bet bet) throws BetException {
    return betDao.add(bet);
  }

  @Override
  public void removeProduct(Product product) throws ProductException {
    productDao.remove(product);
  }
}
