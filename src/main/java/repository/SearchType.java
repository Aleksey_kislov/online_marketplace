package repository;

/**
 * Перечисление типов полей поиска в базе.
 */
public enum SearchType {
  NAME,
  DESCRIPTION,
  ID;
}
