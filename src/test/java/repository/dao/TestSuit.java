package repository.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses(value = {BetDaoTest.class, UserDaoTest.class, ProductDaoTest.class})
@RunWith(Suite.class)
public class TestSuit {
}
