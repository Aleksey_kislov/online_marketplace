package repository.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import repository.dao.exception.ProductException;
import repository.dao.hibernate.HibernateDaoFactory;
import repository.dao.jdbc.OracleDaoFactory;
import repository.pojo.Product;
import repository.pojo.User;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class ProductDaoTest {

  private ProductDao productDao;
  private User user;
  private Product product, product2;

  private static final String SQL_ADD_USER = "INSERT INTO Users(userId,firstName,lastName,login,password) "
                                             + "VALUES(4321,'Pasha','Prokofev','paaaaq','repo')";

  private static final String SQL_DELETE_USER = "DELETE FROM Users WHERE userId = 4321 ";

  private static final String SQL_ADD_PRODUCT = "INSERT INTO Products(id, name, description, userId, betStep, buyNow, startBet, timePerLot,toBets)"
                                                + "VALUES(111, 'notebook', 'розовый', 4321, 0.1, 99.9, 10.1, TO_TIMESTAMP('2011-05-13 07:15:31.123', 'YYYY-MM-DD HH24:MI:SS.FF'), 1)";

  private static final String SQL_DELETE_PRODUCT = "DELETE FROM Products WHERE userId = 4321 ";


  @Parameterized.Parameters
  public static List<ProductDao> isEmptyData() {
    return Arrays.asList(
            new HibernateDaoFactory().getProductDao()
             ,new OracleDaoFactory().getProductDao()
    );
  }

  public ProductDaoTest(ProductDao productDao) {
    this.productDao = productDao;
  }

  @Before
  public void loadDataToTest() throws SQLException {
    OracleDaoFactory.createConnection().prepareStatement(SQL_ADD_USER).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_ADD_PRODUCT).executeUpdate();

    user = new User("Pasha", "Prokofev", "paaaaq", "repo", 4321);

    product = new Product();
    product.setUser(user);
    product.setName("notebook");
    product.setDescription("розовый");
    product.setId(111);
    product.setBetStep(0.1f);
    product.setBuyNow(99.9f);
    product.setStartBet(10.1f);
    product.setTimePerLot(LocalDateTime.of(2011,5,13,7,15,31,123000000));
    product.setToBets(true);

    product2 = new Product();
    product2.setUser(user);
    product2.setName("notebook");
    product2.setDescription("красный");
    product2.setBetStep(0.1f);
    product2.setBuyNow(99.9f);
    product2.setStartBet(10.1f);
    product2.setTimePerLot(LocalDateTime.of(2011,5,13,7,15,31,123000000));
    product2.setToBets(true);
  }

  @After
  public void removeAll() throws SQLException {
    OracleDaoFactory.createConnection().prepareStatement(SQL_DELETE_PRODUCT).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_DELETE_USER).executeUpdate();
  }

  @Test
  public void getAll() throws ProductException {
    List<Product> productList = productDao.getAll();
    Assert.assertTrue(productList.contains(product));
  }

  @Test
  public void getById() throws ProductException {
    Product testProduct = productDao.getById(product.getId());
    Assert.assertEquals(testProduct, product);
  }

  @Test
  public void addTest() throws ProductException {
    product2.setId(productDao.add(product2));
    List<Product> products = productDao.getAll();
    Assert.assertTrue(products.contains(product2));
  }

  @Test
  public void update() throws ProductException {
    String name = "Приправка.";
    product.setName(name);

    productDao.update(product);
    Product testProduct = productDao.getById(product.getId());

    Assert.assertEquals(name, testProduct.getName());
  }

  @Test
  public void getByNameString() throws ProductException {
    List<Product> productList = productDao.getByNameString("book");
    Assert.assertTrue(productList.contains(product));
  }

  @Test
  public void getByDescriptionString() throws ProductException {
    List<Product> productList = productDao.getByDescriptionString("зов");
    Assert.assertTrue(productList.contains(product));
  }

  @Test
  public void getUserProducts() throws ProductException {
    List<Product> productList = productDao.getUserProducts(user);
    Assert.assertTrue(productList.contains(product));
  }
}