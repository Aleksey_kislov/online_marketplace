package repository.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import repository.dao.exception.UserException;
import repository.dao.hibernate.HibernateDaoFactory;
import repository.dao.jdbc.OracleDaoFactory;
import repository.pojo.User;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class UserDaoTest {
  private static User user, user1;

  private static final String SQL_ADD_USER = "INSERT INTO Users(userId,firstName,lastName,login,password) "
                                             + "VALUES(1234,'Alex','Born','alex1000','qwerty123')";
  private static final String SQL_ADD_USER_2 = "INSERT INTO Users(userId,firstName,lastName,login,password) "
                                               + "VALUES(123,'Max','Trap','maxTrap','qwerty321')";

  private static final String SQL_DELETE_USER = "DELETE FROM Users WHERE userId = 1234 ";
  private static final String SQL_DELETE_USER_2 = "DELETE FROM Users WHERE userId = 123 ";
  private static final String SQL_DELETE_USER_WHERE_LOGIN = "DELETE FROM Users WHERE login = 'login' ";

  private static UserDao userDao;


  @Parameterized.Parameters
  public static List<UserDao> isEmptyData() {
    return Arrays.asList(new HibernateDaoFactory().getUserDao(), new OracleDaoFactory().getUserDao());
  }

  public UserDaoTest(UserDao userDao) {
    UserDaoTest.userDao = userDao;
  }

  @Before
  public void loadUser() {
    try (Statement statement = OracleDaoFactory.createConnection().createStatement()) {
      statement.execute(SQL_ADD_USER);
      statement.execute(SQL_ADD_USER_2);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    user = new User("Alex", "Born", "alex1000", "qwerty123", 1234);
    user1 = new User("Max", "Trap", "maxTrap", "qwerty321", 123);
  }

  @After
  public void deleteLoadedUser() throws UserException, SQLException {
    try (Statement statement = OracleDaoFactory.createConnection().createStatement()) {
      statement.execute(SQL_DELETE_USER);
      statement.execute(SQL_DELETE_USER_2);
      statement.execute(SQL_DELETE_USER_WHERE_LOGIN);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void getAll() throws UserException {
    List<User> userList = userDao.getAll();
    Assert.assertTrue(userList.contains(user));
  }

  @Test(expected = UserException.class)
  public void addUserWithDuplicateLogin() throws UserException {
    userDao.add(user);
  }

  @Test
  public void addUserTest() throws UserException {
    User testUser = new User("123", "123", "login", "099");
    userDao.add(testUser);
    User testUser2 = userDao.getByLogin("login");
    Assert.assertEquals(testUser.getLogin(), testUser2.getLogin());
    Assert.assertEquals(testUser.getFirstName(), testUser2.getFirstName());
    Assert.assertEquals(testUser.getLastName(), testUser2.getLastName());
    Assert.assertEquals(testUser.getPassword(), testUser2.getPassword());
  }

  @Test
  public void getById() throws UserException {
    User user1 = userDao.getById(user.getUserId());
    Assert.assertEquals(user.getLogin(), user1.getLogin());
  }

  @Test(expected = UserException.class)
  public void getByInvalidId() throws UserException {
    userDao.getById(-1);
  }

  @Test
  public void getByLogin() throws UserException {
    User user1 = userDao.getByLogin(user.getLogin());
    Assert.assertEquals(user.getLogin(), user1.getLogin());
  }

  @Test(expected = UserException.class)
  public void getByInvalidLogin() throws UserException {
    User user = userDao.getByLogin("slaklsa");
  }

  @Test
  public void updateTest() throws UserException {
    user1.setFirstName("Popup");
    userDao.update(user1);
    Assert.assertEquals(user1.getFirstName(), userDao.getById(user1.getUserId()).getFirstName());
  }
}