package repository.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import repository.dao.exception.BetException;
import repository.dao.hibernate.HibernateDaoFactory;
import repository.dao.jdbc.OracleDaoFactory;
import repository.pojo.Bet;
import repository.pojo.Product;
import repository.pojo.User;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class BetDaoTest {

  private static final String SQL_ADD_USER = "INSERT INTO Users(userId,firstName,lastName,login,password) "
                                             + "VALUES(9999,'Alex','Born','login1','qwertypop')";
  private static final String SQL_ADD_PRODUCT = "INSERT INTO Products(id,name,userId) VALUES(999,'table',9999)";
  private static final String SQL_ADD_BET = "INSERT INTO Bets(userId,productId,bet,betId) VALUES(9999,999,100,99)";

  private static final String SQL_DELETE_USER = "DELETE FROM Users WHERE userId = 9999 ";
  private static final String SQL_DELETE_PRODUCT = "DELETE FROM Products WHERE id = 999 ";
  private static final String SQL_DELETE_BET = "DELETE FROM Bets WHERE betId = 99 ";
  private static final String SQL_DELETE_BET_WHERE = "DELETE FROM Bets WHERE userId = 9999 ";

  private User user;
  private Product product;
  private Bet bet, testBet;

  private BetDao betDao;

  @Parameterized.Parameters
  public static List<BetDao> isEmptyData() {
    return Arrays.asList(new HibernateDaoFactory().getBetDao(),
            new OracleDaoFactory().getBetDao());
  }

  public BetDaoTest(BetDao betDao) {
    this.betDao = betDao;
  }

  @Before
  public void loadDataToDB() throws SQLException {
    OracleDaoFactory.createConnection().prepareStatement(SQL_ADD_USER).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_ADD_PRODUCT).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_ADD_BET).executeUpdate();

    user = new User("Alex", "Born", "login1", "qwertypop", 9999);

    product = new Product();
    product.setId(999);
    product.setName("table");
    product.setUser(user);

    bet = new Bet(9999, 999, 100, 99);
    testBet = new Bet();
    testBet.setUserId(9999);
    testBet.setProductId(999);
    testBet.setBet(99.9F);
    testBet.setUser(user);
    testBet.setProduct(product);
  }

  @After
  public void deleteDataFromDB() throws SQLException {
    OracleDaoFactory.createConnection().prepareStatement(SQL_DELETE_BET_WHERE).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_DELETE_BET).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_DELETE_PRODUCT).executeUpdate();
    OracleDaoFactory.createConnection().prepareStatement(SQL_DELETE_USER).executeUpdate();
  }

  @Test
  public void add() throws BetException {
    testBet.setBetId(betDao.add(testBet));
    List<Bet> betList = betDao.getByProductId(testBet.getProductId());
    Assert.assertTrue(betList.contains(testBet));
  }

  @Test
  public void getByProduct() throws BetException {
    List<Bet> betList = betDao.getByProduct(product);
    Assert.assertTrue(betList.contains(bet));
  }

  @Test
  public void getByProductId() throws BetException {
    List<Bet> betList = betDao.getByProductId(product.getId());
    Assert.assertTrue(betList.contains(bet));
  }

  @Test
  public void getByUser() throws BetException {
    List<Bet> betList = betDao.getByUser(user);
    Assert.assertTrue(betList.contains(bet));
  }

  @Test
  public void getByUserId() throws BetException {
    List<Bet> betList = betDao.getByUserId(user.getUserId());
    Assert.assertTrue(betList.contains(bet));
  }
}