--------------------------------------------------------
--  File created - �������-������-06-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence BETS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MARKETPLACE"."BETS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PRODUCTS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MARKETPLACE"."PRODUCTS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 81 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USERSIDS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MARKETPLACE"."USERSIDS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 401 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table BETS
--------------------------------------------------------

  CREATE TABLE "MARKETPLACE"."BETS" 
   (	"USERID" NUMBER, 
	"PRODUCTID" NUMBER, 
	"BET" FLOAT(126), 
	"BETID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table PRODUCTS
--------------------------------------------------------

  CREATE TABLE "MARKETPLACE"."PRODUCTS" 
   (	"ID" NUMBER, 
	"DESCRIPTION" VARCHAR2(1000 BYTE), 
	"NAME" VARCHAR2(20 BYTE), 
	"STARTBET" FLOAT(126), 
	"BETSTEP" FLOAT(126), 
	"TIMEPERLOT" TIMESTAMP (6), 
	"BUYNOW" FLOAT(126), 
	"TOBETS" NUMBER, 
	"USERID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "MARKETPLACE"."USERS" 
   (	"FIRSTNAME" VARCHAR2(20 BYTE), 
	"LASTNAME" VARCHAR2(20 BYTE), 
	"LOGIN" VARCHAR2(20 BYTE), 
	"PASSWORD" VARCHAR2(20 BYTE), 
	"USERID" NUMBER
   ) ;
REM INSERTING into MARKETPLACE.BETS
SET DEFINE OFF;
REM INSERTING into MARKETPLACE.PRODUCTS
SET DEFINE OFF;
Insert into MARKETPLACE.PRODUCTS (ID,DESCRIPTION,NAME,STARTBET,BETSTEP,TIMEPERLOT,BUYNOW,TOBETS,USERID) values ('2',null,'����������',null,null,null,null,'0','60');
REM INSERTING into MARKETPLACE.USERS
SET DEFINE OFF;
Insert into MARKETPLACE.USERS (FIRSTNAME,LASTNAME,LOGIN,PASSWORD,USERID) values ('Pol','Fox','fox_pol','property','60');
--------------------------------------------------------
--  DDL for Index PRODUCTS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MARKETPLACE"."PRODUCTS_PK" ON "MARKETPLACE"."PRODUCTS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index BETS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MARKETPLACE"."BETS_PK" ON "MARKETPLACE"."BETS" ("BETID") 
  ;
--------------------------------------------------------
--  DDL for Index USERS_PK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "MARKETPLACE"."USERS_PK1" ON "MARKETPLACE"."USERS" ("USERID") 
  ;
--------------------------------------------------------
--  DDL for Index LOGIN_UNIQUE
--------------------------------------------------------

  CREATE UNIQUE INDEX "MARKETPLACE"."LOGIN_UNIQUE" ON "MARKETPLACE"."USERS" ("LOGIN") 
  ;
--------------------------------------------------------
--  DDL for Trigger BETS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MARKETPLACE"."BETS_TRG" 
BEFORE INSERT ON BETS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.BETID IS NULL THEN
      SELECT BETS_SEQ.NEXTVAL INTO :NEW.BETID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "MARKETPLACE"."BETS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PRODUCTS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MARKETPLACE"."PRODUCTS_TRG" 
BEFORE INSERT ON PRODUCTS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT PRODUCTS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "MARKETPLACE"."PRODUCTS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USERS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MARKETPLACE"."USERS_TRG" 
BEFORE INSERT ON USERS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.USERID IS NULL THEN
      SELECT USERSIDS_SEQ.NEXTVAL INTO :NEW.USERID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "MARKETPLACE"."USERS_TRG" ENABLE;
--------------------------------------------------------
--  Constraints for Table BETS
--------------------------------------------------------

  ALTER TABLE "MARKETPLACE"."BETS" ADD CONSTRAINT "BETS_PK" PRIMARY KEY ("BETID") ENABLE;
  ALTER TABLE "MARKETPLACE"."BETS" MODIFY ("BETID" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."BETS" MODIFY ("BET" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."BETS" MODIFY ("PRODUCTID" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."BETS" MODIFY ("USERID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "MARKETPLACE"."USERS" ADD CONSTRAINT "LOGIN_UNIQUE" UNIQUE ("LOGIN") ENABLE;
  ALTER TABLE "MARKETPLACE"."USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("USERID") ENABLE;
  ALTER TABLE "MARKETPLACE"."USERS" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."USERS" MODIFY ("LASTNAME" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."USERS" MODIFY ("USERID" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."USERS" MODIFY ("FIRSTNAME" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."USERS" MODIFY ("LOGIN" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PRODUCTS
--------------------------------------------------------

  ALTER TABLE "MARKETPLACE"."PRODUCTS" ADD CONSTRAINT "PRODUCTS_CHK1" CHECK (TOBETS IN (0,1)) ENABLE;
  ALTER TABLE "MARKETPLACE"."PRODUCTS" ADD CONSTRAINT "PRODUCTS_PK" PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "MARKETPLACE"."PRODUCTS" MODIFY ("USERID" NOT NULL ENABLE);
  ALTER TABLE "MARKETPLACE"."PRODUCTS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table BETS
--------------------------------------------------------

  ALTER TABLE "MARKETPLACE"."BETS" ADD CONSTRAINT "BET_PRODUCT_ID" FOREIGN KEY ("PRODUCTID")
	  REFERENCES "MARKETPLACE"."PRODUCTS" ("ID") ENABLE;
  ALTER TABLE "MARKETPLACE"."BETS" ADD CONSTRAINT "BET_USER_ID" FOREIGN KEY ("USERID")
	  REFERENCES "MARKETPLACE"."USERS" ("USERID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PRODUCTS
--------------------------------------------------------

  ALTER TABLE "MARKETPLACE"."PRODUCTS" ADD CONSTRAINT "USERIDS" FOREIGN KEY ("USERID")
	  REFERENCES "MARKETPLACE"."USERS" ("USERID") ENABLE;
